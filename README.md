# ecollect-docker
## Introduction

Build a simple laravel development environment with docker-compose. Compatible with Windows(WSL2), macOS(M1) and Linux.

## Usage

1. ecollect-dockerのclone
2. ecollect-dockerディレクトリ直下に、`ecollect_admin` をclone
3. dockerを起動

```bash
$ docker-compose up -d
```
4. appコンテナに入ってLaravelのセットアップをしてください

```bash
$ docker-compose exec app bash

# cd ecollect_admin/
# composer install
# cp -p .env.example .env
# php aritsan key:generate
# php aritsan migrate --seed
```

http://localhost:8081
↑
ecollect_admin画面


以下、現在コメントアウト

http://localhost:8082
↑
ecollect_member画面


## Directory structures

```bash
/ecollect-docker
 ├── ecollect_admin
 ├── ecollect_member
 └── infra                : Docker用の管理ファイル
```


## Container structures

```bash
├── app
├── web
└── db
```

### app container

- Base image
  - [php](https://hub.docker.com/_/php):8.0-fpm-bullseye
  - [composer](https://hub.docker.com/_/composer):2.1

### web container

- Base image
  - [nginx](https://hub.docker.com/_/nginx):1.20-alpine
  - [node](https://hub.docker.com/_/node):16-alpine

### db container

- Base image
  - [mysql/mysql-server](https://hub.docker.com/r/mysql/mysql-server):8.0
